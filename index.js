import {initDB} from './src/data_access/Config.js'
import express from 'express'
import bodyParser from 'body-parser'
import {addNewRow} from './src/logic/Product_Logic.js'

initDB()
const app = express()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

app.post('/newRow', (req,res)=>{
    const product_name = req.body.product_name
    const address = req.body.address
    const company = req.body.company
    const price = req.body.price
    addNewRow(product_name, address, company, price)
})

app.listen(3002);