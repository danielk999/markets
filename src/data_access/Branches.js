import Sequelize from 'sequelize'

let branches;

export const defineBranchTable = (sequelize) =>{
        return sequelize.define('Branches',{
            id:{
                type:Sequelize.DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            address:{
                type:Sequelize.DataTypes.STRING
            },
            company:{
                type:Sequelize.DataTypes.STRING
            }
        }
    )
}

export const setBranchTable = (branches_initialized) =>{
    branches = branches_initialized
}

export const checkBranchExists = async (address, company) =>{
    const count = await branches.count({where: {address:address, company:company}})
    
    return count > 0
}

export const insertNewBranch = async (address, company) =>{
    const new_branch = await branches.create({address:address, company:company})
    return new_branch.id
}

export const getBranch = async(address, company) =>{
    return await branches.findOne({where:{address:address, company:company}})
}