import Sequelize from 'sequelize'

let products;

export const defineProductTable = (sequelize) =>{
        return sequelize.define('Products',{
            id:{
                type:Sequelize.DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            name:{
                type:Sequelize.DataTypes.STRING
            }
        }
    )
}

export const setProductTable = (products_initialized) =>{
    products = products_initialized
}

export const checkProductExists = async (product_name) =>{
    const count = await products.count({where: {name:product_name}})

    return count > 0
}

export const insertNewProduct = async (product_name) =>{
    const new_product = await products.create({name:product_name})
    return new_product.id
}

export const getProduct = async(product_name) =>{
    return await products.findOne({where:{name:product_name}})
}