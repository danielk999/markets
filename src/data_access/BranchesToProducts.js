import Sequelize from 'sequelize'

let BranchesToProducts;

export const defineBranchesToProductsTable = (sequelize, Products, Branches) =>{
        BranchesToProducts =  sequelize.define('BranchesToProducts',{
            ProductId:{
            type: Sequelize.DataTypes.INTEGER,
            references: {
                model: Products,
                key: 'id'
            }
            },
            BranchId:{
                type: Sequelize.DataTypes.INTEGER,
                references: {
                    model: Branches,
                    key: 'id'
                }
            },
            price:{
                type:Sequelize.DataTypes.FLOAT
            }
        })
    return BranchesToProducts
}

export const checkBranchesToProductsExists = async (product_id, branch_id) =>{
    const count = await BranchesToProducts.count({where: {ProductId:product_id, BranchId:branch_id}})
    
    return count > 0
}

export const insertNewBranchesToProducts = async (product_id, branch_id, price) =>{
    await BranchesToProducts.create({ProductId:product_id, BranchId:branch_id,price:price})
}

export const updateBranchesToProducts = (product_id, branch_id,price) =>{
    BranchesToProducts.findOne({ProductId:product_id, BranchId:branch_id})
    .then(single_relation =>{
        single_relation.update({price:price})
    })
}

export const getPrice = async(product_id, branch_id) =>{
    return await BranchesToProducts.findOne({where:{ProductId:product_id, BranchId:branch_id}})
}

export const drop = async () =>{
    await BranchesToProducts.drop()
}