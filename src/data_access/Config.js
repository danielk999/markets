import Sequelize from 'sequelize'
import {defineProductTable , setProductTable} from './Products.js'
import {defineBranchTable, setBranchTable} from './Branches.js'
import {defineBranchesToProductsTable} from './BranchesToProducts.js'

const sequelize = new Sequelize('BA1S83OQWDZX9OOJ', 'ADMIN', 'Dk03072023!!', {
    dialect: 'oracle',
    dialectOptions: {
    connectString: '(description= (retry_count=20)(retry_delay=3)(address=(protocol=tcps)(port=1522)(host=adb.il-jerusalem-1.oraclecloud.com))(connect_data=(service_name=ga9776b9c310342_ba1s83oqwdzx9ooj_high.adb.oraclecloud.com))(security=(ssl_server_dn_match=yes)))'
    }
});


const Products = defineProductTable(sequelize)
const Branches =  defineBranchTable(sequelize)
const BranchesToProducts = defineBranchesToProductsTable(sequelize, Products, Branches)

Products.belongsToMany(Branches, { through: BranchesToProducts })
Branches.belongsToMany(Products, { through: BranchesToProducts })

setProductTable(Products)
setBranchTable(Branches)

export const initDB = () =>{

    sequelize.sync()
    .then(() => {
        console.log('Tables created successfully!');
    })
    .catch((error) => {
        console.error('Error creating tables:', error);
    });
}

