import {checkProductExists, insertNewProduct, getProduct} from '../data_access/Products.js'
import {checkBranchExists, insertNewBranch, getBranch} from '../data_access/Branches.js'
import {checkBranchesToProductsExists, insertNewBranchesToProducts, updateBranchesToProducts,drop} from '../data_access/BranchesToProducts.js'

export const addNewRow = async (product_name, address, company, price) =>{
    let product_id;
    let branch_id;
    if(!await checkProductExists(product_name)){
        product_id = await insertNewProduct(product_name)
    }else{
        const product = await getProduct(product_name)
        product_id = product.dataValues.id
    }

    if(!await checkBranchExists(address, company)){
        branch_id = await insertNewBranch(address, company)
    }else{
        const branch = await getBranch(address, company)
        branch_id = branch.dataValues.id
    }

    if(!await checkBranchesToProductsExists(product_id,branch_id)){
        await insertNewBranchesToProducts(product_id, branch_id, price)
    }else{
        updateBranchesToProducts(product_id, branch_id, price)
    }
}

