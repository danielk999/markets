from flask import Flask, request
from routes.receipts import to_products_and_prices_array
from flask_cors import CORS

app = Flask(__name__)


CORS(app, support_credentials=True)

@app.route('/receipts', methods=['POST'])
def receipts():
    data = request.get_json()

    image = data.get('base64')
    image_final64 = image.split(',')[1]

    return to_products_and_prices_array(image_final64)


@app.route("/")
def hi():
    return "hi"
