from DocumentAlgo.inline_text_extraction_demo import algo

import base64
from io import BytesIO
from PIL import Image
import re
import requests

def to_products_and_prices_array(data):
   decoded = base64.b64decode(data)
   image_bytes = BytesIO(decoded)
   image = Image.open(image_bytes)

   image.save("final.jpeg")
   response = algo()
   lines = response.get("pages")[0].get("lines")
   print(lines)

   startIndex=0
   endIndex=0
   for index, line in enumerate(lines):
     if(validate_start(line.text)):
        startIndex=index-1
     if (validate_end(line.text)):
        endIndex = index-1

   rec = lines[startIndex:endIndex]

   for product in range(0, len(rec), 2):
      cell1 = rec[product]
      cell2 = rec[product + 1] if product + 1 < len(rec) else None

      jso = {
         'product_name': cell1,
         'adress': 'hhhhh',
         'company': 'Target',
         'price': cell2
      }

      requests.post("http://localhost:3002", json=jso)





def validate_end(input_string):
   forbiddenWords = ['tax','cost','price','total','sum']

   for word in forbiddenWords:
      if (word in input_string):
         return True
   return False

def validate_start(input_string):
   pattern = r'^[0-9.$]+$'
   if re.match(pattern, input_string):
      return True
   return False