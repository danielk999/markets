import React, { useState, useEffect } from 'react';
import '../index.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight, faChevronLeft, faCircle, faCheckCircle, faPlus } from '@fortawesome/free-solid-svg-icons';

const GroceryList = () => {
	// HINT: each "item" in our list names a name, a boolean to tell if its been completed, and a quantity
    const [items, setItems] = useState([		
	]);

	const [inputValue, setInputValue] = useState('');

	const handleAddButtonClick = () => {
		const newItem = {
			itemName: inputValue,
			quantity: 1			
		};

		const newItems = [...items, newItem];
        if(newItem.itemName!=""){
            setItems(newItems);
		    setInputValue('');		    
        }
		
	};

	const handleQuantityIncrease = (index) => {
		const newItems = [...items];
		newItems[index].quantity++;
		setItems(newItems);		
	};

	const handleQuantityDecrease = (index) => {
		const newItems = [...items];
        if(newItems[index].quantity>0){            
            newItems[index].quantity--;
            setItems(newItems);		    
        }
	};

	const toggleComplete = (index) => {
		const newItems = [...items];
		newItems.splice(index,1);
		setItems(newItems);
        calculateTotal();
	};

	const calculateTotal = () => {
		const totalItemCount = items.reduce((total, item) => {
			return total + item.quantity;
		}, 0);		
        return totalItemCount
	};

    const check=()=>{
        console.log(items);
    }

	return (
		<div className='app-background'>
			<div className='main-container'>
				<div className='add-item-box'>
					<input value={inputValue} onChange={(event) => setInputValue(event.target.value)} className='add-item-input' placeholder='Add an item...' />
					<FontAwesomeIcon icon={faPlus} onClick={() => handleAddButtonClick()} />
				</div>
				<div className='item-list'>
					{items.map((item, index) => (
						<div className='item-container'>
							<div className='item-name' onClick={() => toggleComplete(index)}>
								{item.isSelected ? (
									<>
										<FontAwesomeIcon icon={faCheckCircle} />
										<span className='completed'>{item.itemName}</span>
									</>
								) : (
									<>
										<FontAwesomeIcon icon={faCircle} />
										<span>{item.itemName}</span>
									</>
								)}
							</div>
							<div className='quantity'>
								<button>
									<FontAwesomeIcon icon={faChevronLeft} onClick={() => handleQuantityDecrease(index)} />
								</button>
								<span> {item.quantity} </span>
								<button>
									<FontAwesomeIcon icon={faChevronRight} onClick={() => handleQuantityIncrease(index)} />
								</button>
							</div>
						</div>
					))}
				</div>
				<div className='total'>Total: {calculateTotal()}</div>
                <button
            onClick={(e) => {
              e.preventDefault()
              check();
            }}
            className="btn btn-primary"
          >
            check prices
          </button>
			</div>
		</div>
	);
};

export default GroceryList;