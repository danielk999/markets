import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import Profile from './components/Profile'
import React, { useState } from 'react'
import GroceryList from './components/GroceryList';
export default function App() {
  const [camera, setCamera] = useState(false);
  return (
    <div className="container mt-5">
      <div>
        {
          camera ?
          (<Profile
            setCamera={setCamera} />):
          (<button
            onClick={(e) => {
              setCamera(true)
            }}
            className="btn btn-primary"
          >
            recipt photo
          </button>)
        }
        <GroceryList></GroceryList>
      </div> 
    </div>
  )
};