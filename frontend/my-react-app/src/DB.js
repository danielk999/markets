const db = {
    products:[
        { 
            id: '1',
            name: 'milk'
        },
        { 
            id: '2',
            name: 'eggs'
        },
        { 
            id: '3',
            name: 'bread'
        }
    ],
    supermarkets:[
        { 
            id: '1',
            name: 'milk'
        },
        { 
            id: '2',
            name: 'eggs'
        },
        { 
            id: '3',
            name: 'bread'
        }
    ],
    prices:[
        { 
            id: '1',
            name: 'milk'
        },
        { 
            id: '2',
            name: 'eggs'
        },
        { 
            id: '3',
            name: 'bread'
        }
    ]
};

export default db;