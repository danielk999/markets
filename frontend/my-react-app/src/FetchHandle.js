const url = 'http://localhost:5000/';
const requests={
    picture:
        {
            post: async (value)=>{
                try {
                    console.log(value);
                    const response = await fetch(url+'receipts', {
                      method: 'POST',
                      headers: {
                        'Content-Type': 'application/json',
                        // Add any other headers you need, such as authentication token
                      },
                      body: JSON.stringify({ 'base64': value }), // Replace with your data object
                    });
              
                    const data = await response.json();
                    return data;
                  } catch (error) {
                    console.error('Error:', error);
                  }
            }
        }
    ,
    products: {},
    supermarkets:{

    },
    price:{},
    test:{
      post: async ()=>{
        console.log('test');
        try {            
            const response = await fetch(url, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                // Add any other headers you need, such as authentication token
              },
              body: JSON.stringify({ 'base64': 'value' }),
            });
      
            const data = await response.json();
            return data;
          } catch (error) {
            console.error('Error:', error);
          }
      }
    }
}

export default requests;